﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocumentAnomalyDetection.Core
{
    public class Page
    {
        public decimal page { get; set; }
        public decimal angle { get; set; }
        public decimal width { get; set; }
        public decimal height { get; set; }
        public string unit { get; set; }

        public List<Line> lines { get; set; }
    }
}

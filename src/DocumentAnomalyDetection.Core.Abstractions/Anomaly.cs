﻿
namespace DocumentAnomalyDetection.Core
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public sealed class Anomaly
    {
        public string Code 
        { 
            get;
            set; 
        }

        public string Message
        { 
            get;
            set; 
        }
    }
}

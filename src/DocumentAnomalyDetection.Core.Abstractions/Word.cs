﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocumentAnomalyDetection.Core
{
    public class Word
    {
        public List<decimal> boundingBox { get; set; }

        public string text { get; set; }

        public decimal confidence { get; set; }
    }
}

﻿
namespace DocumentAnomalyDetection.Core
{
    using System.Collections.Generic;

    public sealed class ValidationResult
    {
        public bool HasAnomelies
        {
            get; 
            set; 
        }

        public List<Anomaly> Anomalies 
        {
            get; 
            set;
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocumentAnomalyDetection.Core
{
    public class Style
    {
        public decimal confidence { get; set; }
        public string name { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocumentAnomalyDetection.Core
{
    public class Line
    {
        public string text { get; set; }

        public List<Word> words { get; set; }

        public List<decimal> boundingBox { get; set; }

        public Appearance appearance { get; set; }
    }
}

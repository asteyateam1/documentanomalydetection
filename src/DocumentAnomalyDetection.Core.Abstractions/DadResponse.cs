﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocumentAnomalyDetection.Core
{
    public class DadResponse
    {
        public string status { get; set; }

        public AnalyzeResult analyzeResult { get; set; }
    }
}

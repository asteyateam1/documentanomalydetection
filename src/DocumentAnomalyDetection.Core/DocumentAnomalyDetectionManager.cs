﻿
namespace DocumentAnomalyDetection.Core
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using System.Threading;
    using System.Threading.Tasks;

    public sealed class DocumentAnomalyDetectionManager
    {
        public DocumentAnomalyDetectionManager()
        {

        }

        public async Task<ValidationResult> Validate(byte[] file)
        {
            var client = new HttpClient();
            string location = string.Empty;

            client.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", "8a6567f5913b4cd09df13eded0679124");
            var uri = "https://dad.cognitiveservices.azure.com/vision/v3.2/read/analyze?/en/0-/";


            HttpResponseMessage response;
            // getting the files form th OS
            //var file = File.ReadAllBytes(@"C:\Users\abedk\Downloads\1st Empty Page.pdf");
            using (var content = new ByteArrayContent(file))
            {
                content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");

                response = client.PostAsync(uri, content).Result;

                var message = response.Content.ReadAsStringAsync();

                location = response.Headers.GetValues("Operation-Location").First();
            }

            do
            {
                Thread.Sleep(2000);
            }
            while (JsonConvert.DeserializeObject<DadResponse>(client.GetAsync(location).Result.Content.ReadAsStringAsync().Result).status == "Running");

            var result = JsonConvert.DeserializeObject<DadResponse>(client.GetAsync(location).Result.Content.ReadAsStringAsync().Result);

            File.WriteAllText(@"D:\test2.json", JsonConvert.SerializeObject(result.analyzeResult.readResults));

            var whitePages = result.analyzeResult.readResults.Where(x => x.lines.Count == 0).ToList();

            if (whitePages.Count > 0)
            {
                var pagesNumbers = string.Join(',', whitePages.Select(x => x.page));

                return new ValidationResult()
                {
                    HasAnomelies = true,
                    Anomalies = new List<Anomaly>()
                    {
                        new Anomaly
                        {
                            Code = "WhiteSpace",
                            Message = "Pages: " + pagesNumbers + " are empty"
                        }
                    }
                };
            }

            var random = new Random();

            int index = random.Next(validationResults.Count);


            return await Task.FromResult(validationResults[index]);
        }
    }
}

﻿
namespace Microsoft.Extensions.DependencyInjection
{
    using DocumentAnomalyDetection.Core;

    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddCore(this IServiceCollection services)
        {
            services.AddSingleton<DocumentAnomalyDetectionManager>();

            return services;
        }
    }
}

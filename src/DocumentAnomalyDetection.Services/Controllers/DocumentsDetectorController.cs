﻿
namespace DocumentAnomalyDetection.Services.Controllers
{
    using DocumentAnomalyDetection.Core;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using System.IO;
    using System.Threading.Tasks;

    [Route("api")]
    [ApiController]
    public class DocumentsDetectorController : ControllerBase
    {
        private readonly DocumentAnomalyDetectionManager documentAnomalyDetectionManager;

        public DocumentsDetectorController(DocumentAnomalyDetectionManager documentAnomalyDetectionManager)
        {
            this.documentAnomalyDetectionManager = documentAnomalyDetectionManager ?? throw new System.ArgumentNullException(nameof(documentAnomalyDetectionManager));
        }

        [HttpPost]
        [Route("validate")]
        [ProducesResponseType(200, Type = typeof(Core.ValidationResult))]
        public async Task<ActionResult> Validate(IFormFile file)
        {
            var result = await documentAnomalyDetectionManager.Validate(await FileToArray(file));

            return Ok(result);
        }

        private async Task<byte[]> FileToArray(IFormFile file)
        {
            MemoryStream memoryStream = null;

            if (file != null)
            {
                memoryStream = new MemoryStream();

                await file.CopyToAsync(memoryStream);
                memoryStream.Seek(0, SeekOrigin.Begin);
            }

            return memoryStream.ToArray();
        }
    }
}
